import React from "react";
import { useState } from "react";
function Nav() {
  const [showLinks, setShowLinks] = useState(false);
  const handleShowLinks = () => {
    setShowLinks(!showLinks);
    console.log(showLinks);
  };
  return (
    <nav className={`navbar ${showLinks ? "show-nav" : "hide-nav"} `}>
        <div className="navbar_logo">logo</div>
        <ul className="navbar_links">
          <li className="navbar_items">
            <a href="#" className="navbar_link">
              accueil
            </a>
          </li>
          <li className="navbar_items">
            <a href="#" className="navbar_link">
              A propos
            </a>
          </li>
          <li className="navbar_items">
            <a href="#" className="navbar_link">
              Portfolio
            </a>
          </li>
          <li className="navbar_items">
            <a href="#" className="navbar_link">
              Services
            </a>
          </li>
          <li className="navbar_items">
            <a href="#" className="navbar_link">
              Contact
            </a>
          </li>
        </ul>
        <button className="navbar-burger" onClick={handleShowLinks}>
          <span className="burger-bar"></span>
        </button>
      </nav> 
  );

  
}

export default Nav;
